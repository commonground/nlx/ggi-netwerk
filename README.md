This repository contains the deployment configuration of NLX to GGI-Netwerk

Kubernetes credentials to the dv-ggi.k8s.local are required

A namespace named `nlx-demo` is created on the cluster for this deployment.

The directory UI needs a certificate to prevent browser warnings. This certificate is stored in the secret `directory-ui-ingress-tls` and must be present before deploying.

The following dependencies should be installed on the Kubernetes cluster:

- [cert-manager](https://cert-manager.io/)
- [zalando postgres operator](https://github.com/zalando/postgres-operator)
- [traefik](https://doc.traefik.io/traefik/providers/kubernetes-ingress/)

In case the secret needs to be recreated you can do so by executing the following command (the certificate and key are both stored in the 1Password vault of Team NLX):

```bash
kubectl create secret tls -n nlx-demo directory-ui-ingress-tls --cert={path of the certificate file} --key={path of the key file}
```

The required charts can be deployed using the following commands:

```bash
helm upgrade --install -n nlx-demo directory deployment/directory -f deployment/directory/values-demo.yaml \
helm upgrade --install -n nlx-demo rvrd deployment/rvrd -f deployment/rvrd/values-demo.yaml \
helm upgrade --install -n nlx-demo vergunningsoftware-bv deployment/vergunningsoftware-bv -f deployment/vergunningsoftware-bv/values-demo.yaml
```
NLX uses 3 IP addresses on GGI-netwerk, the routing of these IP addresses to the corresponding Kubernetes service is configured by DirectVPS. The configuration is documented in the drawio diagram `setup.drawio` available in this repository, to change the routing configuration a request can be sent to support@directvps.nl
Please note that the routing configured by DirectVPS only allows traffic over the ports 443 and 8443.

NLX claimed the subdomain `nlx`. More subdomains can be claimed at the [Servicecentrum Gemeenten](https://scgemeenten.topdesk.net), the login details are available in the 1Password vault of Team NLX.
The subdomain `nlx` can be configured by a [portal hosted on GGI netwerk](`https://dnsbeheer.ggi-netwerk.net`). The credentials are available in the 1Password vault of Team NLX.

The cluster provided by DirectVPS is connected to GGI network but does not use the DNS servers of GGI-Netwerk, it is important that the cluster uses these DNS servers because it will enable the cluster to resolve the Inway of RvRD which is configured to run on `inway.rvrd.demo.nlx.ggi-netwerk.net`. 
Without this DNS server the directory monitor, running in the same cluster, is unable to health check the Inway.
CoreDNS is running on the cluster, and it allows custom nameservers to be [configured](https://kubernetes.io/docs/tasks/administer-cluster/dns-custom-nameservers/#configuration-of-stub-domain-and-upstream-nameserver-using-coredns)
The default configuration was extended with te following section:
```
ggi-netwerk.net:53 {
    errors
    cache 30
    forward . 159.46.73.177
}
```

For CI/CD pipeline to work, a gitlab agent should be installed on the cluster. The folder `./gitlab-agent` contains the kubernetes manifests for the agent which can be installed using `kubectl`.

Start by executing
```
kubectl apply -f ./gitlab-agent/namespace.yaml
```

Next create a secret containing the agent token (replace `{AGENT_TOKEN}` with the obtained token). The agent token can be obtained by navigation to Project->Infrastructure->Kubernetes Clusters and selecting the agent `nlx-demo`
Create te secret by executing
```
kubectl create secret generic -n gitlab-agent-nlx gitlab-agent-nlx-secret --from-literal=token='{AGENT_TOKEN}'
```

Once the secret is created execute
```
kubectl apply -f ./gitlab-agent/deployment.yaml
```

and

```
kubectl apply -f ./gitlab-agent/nlx.yaml
```
